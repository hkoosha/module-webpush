<?php

class WebpushSubscriptionApple extends Entity {

}

class WebpushSubscriptionAppleController extends EntityAPIController {

}

function _webpush_subscription_apple_entity_info() {
  $info = [];

  $info['webpush_subscription_apple'] = [
    'label' => 'WebPush subscription - Apple',
    'entity class' => 'WebpushSubscriptionApple',
    'controller class' => 'WebpushSubscriptionAppleController',
    'views controller class' => 'EntityDefaultViewsController',
    'base table' => 'webpush_subscription_apple',
    'entity keys' => [
      'id' => 'id',
    ],
    'access callback' => 'webpush_subscription_apple_access_callback',
    'admin ui' => [
      'controller class' => 'EntityDefaultUIController',
    ],
    'module' => 'webpush',
    'fieldable' => TRUE,
    'bundles' => [
      'webpush_subscription_apple' => [
        'label' => 'WebPush subscription - Apple',
        'admin' => [
          'path' => 'admin/structure/webpush-subscription-apple/manage',
          'access arguments' => ['administer webpush'],
        ],
      ],
    ],
  ];

  return $info;
}

function _webpush_subscription_apple_entity_property_info() {
  $info = [];

  $info['webpush_subscription_apple']['properties']['id'] = [
    'label' => t('ID', [], ['context' => 'webpush']),
    'description' => t('The subscription ID.', [], ['context' => 'webpush']),
    'type' => 'integer',
    'schema field' => 'id',
  ];
  $info['webpush_subscription_apple']['properties']['created'] = [
    'label' => t('Created', [], ['context' => 'webpush']),
    'description' => t('Creation date', [], ['context' => 'webpush']),
    'type' => 'date',
    'schema field' => 'created',
  ];
  $info['webpush_subscription_apple']['properties']['token'] = [
    'label' => t('Token', [], ['context' => 'webpush']),
    'description' => t('Apple device generated token', [], ['context' => 'webpush']),
    'type' => 'text',
    'schema field' => 'token',
  ];

  return $info;
}

