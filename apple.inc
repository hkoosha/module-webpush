<?php

define('WEBPUSH_APPLE_APN_GATEWAY', 'ssl://gateway.push.apple.com:2195');

function _webpush_subscription_apple_log($a, $tag) {
  $s0 = json_encode(file_get_contents("php://input"), JSON_PRETTY_PRINT);
  $s1 = json_encode($a, JSON_PRETTY_PRINT);
  watchdog('webpush', '@tag => <pre>@data0</pre><br><pre>@data1</pre>', [
    '@data0' => $s0,
    '@data1' => $s1,
    '@tag' => $tag,
  ], WATCHDOG_DEBUG);
}

function _webpush_subscription_apple_package($a) {
  _webpush_subscription_apple_log(func_get_args(), 'packages');
  $v = in_array('v1', $a, TRUE) ? '1' : '2';
  $path = __DIR__ . "/push_$v.zip";
  header('Access-Control-Allow-Origin', '*');
  header("Content-type: application/zip");
  echo file_get_contents($path);
  die;
}

function _webpush_subscription_apple_device() {
  _webpush_subscription_apple_log(func_get_args(), 'devices');

  $args = func_get_args();
  $args = $args[0] ?? [];
  $token = $args[2] ?? '';
  if (empty($token)) {
    watchdog('webpush', 'empty token', [], WATCHDOG_WARNING);
    return;
  }

  if ((new EntityFieldQuery())
      ->entityCondition('entity_type', 'webpush_subscription_apple')
      ->propertyCondition('token', $token)
      ->addMetaData('account', user_load(1))
      ->count()
      ->execute() > 0) {
    // already registered.
    return;
  }

  $entity_type = 'webpush_subscription_apple';
  $entity = entity_create($entity_type, [
    'token' => $token,
    'created' => REQUEST_TIME,
  ]);
  $entity->save();
}

function _webpush_subscription_apple_unsubscribe() {
  _webpush_subscription_apple_log(func_get_args(), 'unsubscribe');

  $args = func_get_args();
  $args = $args[0] ?? [];
  $token = $args[1] ?? '';
  if (empty($token)) {
    watchdog('webpush', 'empty token', [], WATCHDOG_WARNING);
    return;
  }

  $result = (new EntityFieldQuery())
    ->entityCondition('entity_type', 'webpush_subscription_apple')
    ->propertyCondition('token', $token)
    ->addMetaData('account', user_load(1))
    ->range(0, 1)
    ->execute();
  if (isset($result['webpush_subscription_apple'])) {
    reset($result['webpush_subscription_apple']);
    $id = key($result['webpush_subscription_apple']);
    entity_delete('webpush_subscription_apple', $id);
  }
}

function _webpush_subscription_apple_subscribe() {
  _webpush_subscription_apple_log(func_get_args(), 'subscribe');

  $args = func_get_args();
  $args = $args[0] ?? [];
  $token = $args[1] ?? '';
  _webpush_subscription_apple_device([
    NULL,
    NULL,
    $token,
  ]);
}

function _webpush_subscription_apple() {
  if (in_array('packages', func_get_args(), TRUE) || in_array('pushPackages', func_get_args(), TRUE)) {
    _webpush_subscription_apple_package(func_get_args());
  }
  elseif (in_array('devices', func_get_args(), TRUE)) {
    _webpush_subscription_apple_device(func_get_args());
  }
  elseif (in_array('unsubscribe', func_get_args(), TRUE)) {
    _webpush_subscription_apple_unsubscribe(func_get_args());
  }
  elseif (in_array('subscribe', func_get_args(), TRUE)) {
    _webpush_subscription_apple_subscribe(func_get_args());
  }
  else {
    _webpush_subscription_apple_log(func_get_args(), 'log');
  }
  return ['#markup' => 'halo'];
}

function _webpush_subscription_apple_send($token, $title, $message, $url_arg, $action = 'view') {
  if (!is_string($token) || empty($token)) {
    return 'no/bad token';
  }

  $push_apple = WEBPUSH_APPLE_APN_GATEWAY;

  $exported = variable_get('webpush_apple_cert_exported_path', '');
  if (empty($exported)) {
    return 'no exported certificate';
  }

  $passphrase = variable_get('webpush_apple_cert_pass', '');
  if (empty($passphrase)) {
    return 'no passphrase';
  }

  //  $exported = '/a/ApnsPHP/server_certificates_bundle_sandbox.pem';
  $cafile = variable_get('webpush_apple_cert_cafile', NULL);
  $ctx = stream_context_create([
    'ssl' => [
      'verify_peer' => TRUE,
      'cafile' => $cafile,
      'local_cert' => $exported,
      'passphrase' => $passphrase,
    ],
  ]);

  if ($ctx === FALSE) {
    return "stream context error";
  }

  $stream_success = stream_context_set_option($ctx, 'ssl',
    'passphrase', $passphrase);
  if ($stream_success === FALSE) {
    return "stream context set option error, ssl::passphrase";
  }

  /** @noinspection PhpStatementHasEmptyBodyInspection */
  while ($err = openssl_error_string()) {
    // clear
  }

  $fp = stream_socket_client($push_apple, $err, $errstr, 5,
    STREAM_CLIENT_CONNECT, $ctx);
  if ($fp === FALSE) {
    return "connection error, $err::$errstr" . " - " . openssl_error_string();
  }

  $body = [
    'aps' => [
      'alert' => [
        'title' => $title,
        'body' => $message,
        'action' => $action,
      ],
      'url-args' => [$url_arg, '/'],
    ],
  ];
  $payload = json_encode($body);

  // Build the binary notification
  $msg = chr(0) .
    pack('n', 32) .
    pack('H*', $token) .
    pack('n', strlen($payload)) .
    $payload;

  $result = fwrite($fp, $msg, strlen($msg));
  $res = fclose($fp);

  if (!$result) {
    watchdog('webpush', 'apple push delivery error, token: @token', [
      '@token' => $token,
    ], WATCHDOG_ERROR);
  }

  return '';
}

// ============================================================================

function _webpush_subscription_apple_menu() {
  $items['admin/config/services/webpush/apple'] = [
    'type' => MENU_LOCAL_TASK,
    'title' => 'Apple Payload',
    'access arguments' => ['administer webpush'],
    'file' => 'webpush.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['webpush_apple_payload_form'],
  ];

  $items["webpush-apple"] = [
    'title' => 'Apple WebPush Subscriptions / Logging',
    'description' => 'Endpoint to register to push messages for apple family of devices / Logging',
    'page callback' => '_webpush_subscription_apple',
    'file' => 'apple.inc',
    'access callback' => TRUE,
  ];

  return $items;
}

function _webpush_subscription_apple_add_js() {
  drupal_add_js([
    'webpush' => [
      'apple' => [
        'id' => variable_get('webpush_apple_push_id'),
        'domain' => variable_get('webpush_apple_push_domain'),
        'uid' => $GLOBALS['user']->uid,
      ],
    ],
  ], ['type' => 'setting']);
}

// ============================================================================

function _webpush_subscription_apple_device_create_push_package_website_json_file($package_dir, $json) {
  if (!is_dir($package_dir)) {
    throw new Exception("file error, not a directory: package_dir=$package_dir");
  }
  $file = "$package_dir/website.json";
  if (!file_exists($file)) {
    $content = json_encode($json, JSON_PRETTY_PRINT);
    $content = str_replace('\\/', '/', $content);
    if (file_put_contents($file, $content) === FALSE) {
      throw new Exception("file error, could not write website.json: file=$file, content=$content");
    }
  }
}

function _webpush_subscription_apple_device_create_push_package($package_version, $package_dir, $cert_path, $cert_password, $source_files, $apple_pem) {
  $raw_files_ = [
    'icon.iconset/icon_16x16.png',
    'icon.iconset/icon_16x16@2x.png',
    'icon.iconset/icon_32x32.png',
    'icon.iconset/icon_32x32@2x.png',
    'icon.iconset/icon_128x128.png',
    'icon.iconset/icon_128x128@2x.png',
    'website.json',
  ];

  if ($package_version !== 1 && $package_version !== 2) {
    throw new Exception("Invalid push package version: $package_version");
  }
  if (!is_dir($package_dir)) {
    throw new Exception("file error, not a directory: package_dir=$package_dir");
  }

  if (!mkdir("$package_dir/icon.iconset")) {
    throw new Exception("file error, mkdir($package_dir/icon.iconset)");
  }

  foreach ($raw_files_ as $raw_file) {
    if ($raw_file === 'website.json') {
      continue;
    }
    $name = explode('/', $raw_file)[1];
    if (!copy("$source_files/$name", "$package_dir/$raw_file")) {
      throw new RuntimeException("file error, copy($source_files/$name, $package_dir/$raw_file");
    }
  }

  $manifest_data = [];
  foreach ($raw_files_ as $raw_file) {
    $file_contents = file_get_contents("$package_dir/$raw_file");
    if ($file_contents === FALSE) {
      throw new RuntimeException("file_error: file_get_contents($package_dir/$raw_file)");
    }

    $manifest_data[$raw_file] = $package_version === 1
      ? sha1($file_contents)
      : [
        'hashType' => 'sha512',
        'hashValue' => hash('sha512', $file_contents),
      ];
  }

  if (file_put_contents("$package_dir/manifest.json", json_encode((object) $manifest_data)) === FALSE) {
    throw new RuntimeException("file_put_contents($package_dir/manifest.json, json_encode((object) $manifest_data");
  }

  // Load the push notification certificate
  $pkcs12 = file_get_contents($cert_path);
  if ($pkcs12 === FALSE) {
    throw new RuntimeException("file error: file_get_contents($cert_path)");
  }

  $certs = [];
  if (!openssl_pkcs12_read($pkcs12, $certs, $cert_password)) {
    throw new RuntimeException("ssl error: openssl_pkcs12_read(PKCS12, [], $cert_password)");
  }

  $signature_path = "$package_dir/signature";

  $cert_data = openssl_x509_read($certs['cert']);
  if ($cert_data === FALSE) {
    throw new RuntimeException('ssl error: openssl_x509_read(' . $certs['cert'] . ')');
  }

  $private_key = openssl_pkey_get_private($certs['pkey'], $cert_password);
  if ($private_key === FALSE) {
    throw new RuntimeException('ssl error: openssl_pkey_get_private(' . $certs['pkey'] . ', XXX)');
  }

  if (!openssl_pkcs7_sign("$package_dir/manifest.json",
    $signature_path,
    $cert_data,
    $private_key,
    [],
    PKCS7_BINARY | PKCS7_DETACHED,
    $apple_pem ?: NULL)) {
    throw new RuntimeException("ssl error: openssl_pkcs7_sign($package_dir/manifest.json, " .
      "$signature_path, CERT_DATA, PRIVATE_KEY, [], PKCS7_BINARY | PKCS7_DETACHED, $apple_pem )"
    );
  }

  $signature_pem = file_get_contents($signature_path);
  if ($signature_pem === FALSE) {
    throw new RuntimeException("file error: file_get_contents($signature_path)");
  }

  $matches = [];
  if (!preg_match('~Content-Disposition:[^\n]+\s*?([A-Za-z0-9+=/\r\n]+)\s*?-----~', $signature_pem, $matches)) {
    throw new RuntimeException('certificate error, invalid certificate, file does not match expected format.');
  }

  $signature_der = base64_decode($matches[1]);
  if ($signature_der === FALSE) {
    throw new RuntimeException('certificate error, invalid certificate, could not decode as base64.');
  }

  if (file_put_contents($signature_path, $signature_der) === FALSE) {
    throw new RuntimeException("file error: file_put_contents($signature_path, SIGNATURE_DER)");
  }

  $zip_path = "$package_dir.zip";

  // Package files as a zip file
  $zip = new ZipArchive();
  if (!$zip->open("$package_dir.zip", ZIPARCHIVE::CREATE)) {
    throw new RuntimeException("file error: zip->open($package_dir.zip, ZIPARCHIVE::CREATE)");
  }

  $raw_files = $raw_files_;
  $raw_files[] = 'manifest.json';
  $raw_files[] = 'signature';
  foreach ($raw_files as $raw_file) {
    $zip->addFile("$package_dir/$raw_file", $raw_file);
  }

  $zip->close();
  return $zip_path;
}

function _webpush_subscription_apple_admin_form(&$form) {
  $form['apple'] = [
    '#type' => 'fieldset',
    '#title' => t('Apple Push Notification', [], ['context' => 'webpush']),
    '#collapsible' => FALSE,
  ];
  $form['apple']['webpush_apple_push_id'] = [
    '#type' => 'textfield',
    '#title' => t('Id', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_push_id', ''),
    '#description' => t('web.com.company.example as set in apple dev portal', [], ['context' => 'webpush']),
  ];
  $form['apple']['webpush_apple_push_domain'] = [
    '#type' => 'textfield',
    '#title' => t('Domain', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_push_domain', ''),
    '#description' => t('example.company.com as set in apple dev portal', [], ['context' => 'webpush']),
  ];
  $form['apple']['webpush_apple_cert_path'] = [
    '#type' => 'textfield',
    '#title' => t('Certificate File Path', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_cert_path', ''),
    '#description' => t('Taken from apple dev portal.', [
      '#required' => TRUE,
    ]),
  ];
  $form['apple']['webpush_apple_cert_pass'] = [
    '#type' => 'textfield',
    '#title' => t('Certificate Password', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_cert_pass', ''),
  ];
  $form['apple']['webpush_apple_cert_cafile'] = [
    '#type' => 'textfield',
    '#title' => t('CA File', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_cert_cafile', ''),
    '#description' => t('Certificate used to verify APN. You can' .
      ' download it from <a href="https://www.entrustdatacard.com/pages/root-certificates-download">entrust</a>.'
      . ' <b>the one that exactly says Entrust.net Certificate Authority (2048)</b>', [
      '#required' => FALSE,
    ]),
  ];
  $form['apple']['webpush_apple_cert_exported_path'] = [
    '#type' => 'textfield',
    '#title' => t('Result of "<em>openssl pkcs12 -in "Certificate File Path" -clcerts -out output.crt</em>"', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_cert_exported_path', ''),
    '#description' => t('Currently equivalent of this open ssl command in php fails and has to be done outside php. <b>THE KEY FILE MUST BE ENCRYPTED WITH THE KEY SPECIFIED ABOVE</b>', [
      '#required' => TRUE,
    ]),
  ];
}

function webpush_apple_payload_form() {
  $raw_files = [
    'icon_16x16.png',
    'icon_16x16@2x.png',
    'icon_32x32.png',
    'icon_32x32@2x.png',
    'icon_128x128.png',
    'icon_128x128@2x.png',
    'website.json',
  ];
  $raw_files = implode(', ', $raw_files);

  $form['package_dir'] = [
    '#type' => 'textfield',
    '#title' => t('Package directory', [], ['context' => 'webpush']),
    '#description' => t('Will generate the package here.', [
      '#default_value' => '/tmp/x',
      '#required' => TRUE,
    ]),
    '#default_value' => '/tmp/',
  ];
  $form['cert_path'] = [
    '#type' => 'textfield',
    '#title' => t('Certificate Path', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_cert_path', ''),
    '#description' => t('Taken from apple dev portal.', [
      '#required' => TRUE,
    ]),
  ];
  $form['cert_pass'] = [
    '#type' => 'textfield',
    '#title' => t('Certificate Password', [], ['context' => 'webpush']),
    '#default_value' => variable_get('webpush_apple_cert_pass', ''),
    '#required' => TRUE,
  ];
  $form['apple_pem'] = [
    '#type' => 'textfield',
    '#title' => t('AppleWWDRCA.pem file path', [], ['context' => 'webpush']),
    '#description' => t('Find it on <a href="https://developer.apple.com/support/expiration/">Apple</a>'
      . ' or download directly from <a href="https://developer.apple.com/certificationauthority/AppleWWDRCA.cer">here</a>.'
      . ' Please note you have to convert it using the command: openssl x509 -inform der -in AppleWWDRCA.cer -out apple.pem',
      [], ['context' => 'webpush']),
    '#required' => FALSE,
    '#default_value' => '/tmp/apple.pem',
  ];
  $form['source'] = [
    '#type' => 'textfield',
    '#title' => t('Source files (icons and website.json directory)', [], ['context' => 'webpush']),
    '#description' => t('A directory containing the files: [@f]', [
      '@f' => $raw_files,
    ], ['context' => 'webpush']),
    '#required' => TRUE,
    '#default_value' => '/tmp/raw',
  ];

  $form['website'] = [
    '#type' => 'fieldset',
    '#title' => t('website.json file', [], ['context' => 'webpush']),
  ];
  $form['website']['websiteName'] = [
    '#type' => 'textfield',
    '#title' => t('Website Name', [], ['context' => 'webpush']),
    '#required' => TRUE,
    '#default_value' => variable_get('site_name'),
  ];
  $form['website']['websitePushID'] = [
    '#type' => 'textfield',
    '#title' => t('Website Push Id', [], ['context' => 'webpush']),
    '#required' => TRUE,
    '#default_value' => variable_get('webpush_apple_push_id'),
  ];
  $form['website']['allowedDomains'] = [
    '#type' => 'textfield',
    '#title' => t('Allowed Domain, comma separated', [], ['context' => 'webpush']),
    '#required' => TRUE,
    '#default_value' => str_replace('http:', 'https:', $GLOBALS['base_url']) . ', ',
  ];
  $form['website']['urlFormatString'] = [
    '#type' => 'textfield',
    '#title' => t('Url Format String', [], ['context' => 'webpush']),
    '#default_value' => str_replace('http:', 'https:', $GLOBALS['base_url']) . '/webpush-apple/%@/%@',
    '#required' => TRUE,
    '#disabled' => TRUE,
  ];
  $form['website']['webServiceURL'] = [
    '#type' => 'textfield',
    '#title' => t('Web Service URL', [], ['context' => 'webpush']),
    '#required' => TRUE,
    '#default_value' => str_replace('http:', 'https:', $GLOBALS['base_url']),
  ];
  $form['website']['authenticationToken'] = [
    '#type' => 'textfield',
    '#title' => t('Authentication token (16 characters minimum)', [], ['context' => 'webpush']),
    '#required' => TRUE,
    '#default_value' => drupal_hash_base64(random_bytes(512)),
  ];
  $form['actions']['#type'] = 'actions';

  $form['actions']['submit1'] = [
    '#type' => 'submit',
    '#value' => t('Generate Package Version 1'),
  ];

  $form['actions']['submit2'] = [
    '#type' => 'submit',
    '#value' => t('Generate Package Version 2'),
  ];

  $form['#submit'][] = '_webpush_apple_payload_form_submit';
  $form['#theme'] = 'system_settings_form';

  return $form;
}

function _webpush_apple_payload_form_submit(&$f, &$fs) {
  $ver = in_array('submit1', $fs['clicked_button']['#parents'], TRUE) ? 1 : 2;
  $dir = $fs['values']['package_dir'];
  $src = $fs['values']['source'];
  $c_path = $fs['values']['cert_path'];
  $c_pass = $fs['values']['cert_pass'];
  $apple_pem = $fs['values']['apple_pem'];

  $website_json = [];
  foreach ([
             'websiteName',
             'websitePushID',
             'urlFormatString',
             'webServiceURL',
             'authenticationToken',
           ] as $k) {
    $website_json[$k] = $fs['values'][$k];
  }
  $website_json['allowedDomains'] = array_filter(array_map('trim', explode(',', $fs['values']['allowedDomains'])));
  $website_json['webServiceURL'] .= '/webpush-apple';

  $dir = $dir . '/' . time();
  mkdir($dir);

  list($ok, $err_msg, $throwable) = _webpush_apple_payload__gen_package(
    $dir, $website_json, $ver, $c_path, $c_pass, $src, $apple_pem);

  if (!$ok) {
    form_set_error(NULL, $err_msg);
    drupal_set_message($err_msg, 'error');
  }
  else {
    header("Content-type: application/zip");
    header("Content-Disposition: inline; filename=\"push_$ver.zip\"");
    echo $ok;
    die;
  }
}

function _webpush_apple_payload__gen_package($dir, $website_json, $ver, $c_path, $c_pass, $src, $apple_pem) {

  $zip = NULL;
  try {
    _webpush_subscription_apple_device_create_push_package_website_json_file($dir, $website_json);
    $zip = _webpush_subscription_apple_device_create_push_package($ver, $dir, $c_path, $c_pass, $src, $apple_pem);
  }
  catch (Throwable $t) {
    return [NULL, $t->getMessage(), $t];
  }

  $file = file_get_contents($zip);
  return $file === FALSE
    ? [NULL, "could not read generated file: $zip", NULL]
    : [$file, NULL, NULL];
}
